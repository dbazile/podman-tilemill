#!/bin/bash

IMAGE_ID='dbazile/tilemill'
CONTAINER_ID='tilemill'

set -e

cd "$(dirname $0)"


flags=()
cmd=()

while [[ -n "$1" ]]; do
	if [[ "$1" == '--' ]]; then
		shift
		cmd+=( "$@" )
		set --
		break
	else
		flags+=( "$1" )
		shift
	fi
done


set -x

podman build -t "$IMAGE_ID" .
podman run \
	--rm \
	--name "$CONTAINER_ID" \
	-it \
	-p 20008:20008 \
	-p 20009:20009 \
	"${flags[@]}" "$IMAGE_ID" "${cmd[@]}"
