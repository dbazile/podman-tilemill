FROM ubuntu:trusty

RUN set -ex \
	&& apt-get update \
	&& apt-get install -y software-properties-common

RUN set -ex \
	&& add-apt-repository ppa:developmentseed/mapbox \
	&& apt-get update \
	&& apt-get install -y tilemill libmapnik nodejs

EXPOSE 20008 20009

CMD ["node", "/usr/share/tilemill/index.js", "start", "--server=true", "--listenHost=0.0.0.0"]
